package com.kmia.bjs.flightserver.config;

/**
 * 自定义异常枚举类
 */
public enum ErrorEnum {

    // 400系列
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "请求的数据格式不符!"),
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED, "登录凭证过期!"),
    FORBIDDEN(HttpStatus.FORBIDDEN, "抱歉，你无权限访问!"),
    NOT_FOUND(HttpStatus.NOT_FOUND, "请求的资源找不到!"),

    // 500系列
    INTERNAL_SERVER_ERROR(HttpStatus.ERROR, "服务器内部错误!"),
    SERVICE_UNAVAILABLE(HttpStatus.UNAVAILABLE, "服务器正忙，请稍后再试!"),

    // 未知异常
    UNKNOWN(HttpStatus.UNKNOWN, "未知异常!");

    /**
     * 错误码
     */
    private int code;

    /**
     * 错误描述
     */
    private String msg;

    ErrorEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
