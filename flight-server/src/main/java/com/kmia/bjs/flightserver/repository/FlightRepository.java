package com.kmia.bjs.flightserver.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kmia.bjs.flightserver.entity.Flight;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepository extends BaseMapper<Flight> {
}
