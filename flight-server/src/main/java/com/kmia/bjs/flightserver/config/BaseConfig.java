package com.kmia.bjs.flightserver.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @Description 自定义读取配置文件类
 * @Author 4K
 * @Date 2022/5/10 15:44
 * @Version 1.0
 **/
@Slf4j
@Component
public class BaseConfig implements ApplicationRunner {

    @Autowired
    private Environment environment;

    // xml文件存放路径
    public static String XMLPath;
    public static int NFLT_Number = 1001;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        XMLPath = environment.getProperty("filePath.xmlPath");
        if (environment.getProperty("NFLT_Number") != null){
            NFLT_Number = Integer.valueOf(environment.getProperty("NFLT_Number"));
        }

    }
}
