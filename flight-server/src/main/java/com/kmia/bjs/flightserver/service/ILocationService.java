package com.kmia.bjs.flightserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kmia.bjs.flightserver.entity.Location;

/**
 * @Description 机场服务接口
 * @Author 4K
 * @Date 2022/4/29 16:40
 * @Version 1.0
 **/
public interface ILocationService<D> extends IService<Location> {
}
