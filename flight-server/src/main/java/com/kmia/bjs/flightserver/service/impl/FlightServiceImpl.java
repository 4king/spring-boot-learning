package com.kmia.bjs.flightserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kmia.bjs.flightserver.entity.Flight;
import com.kmia.bjs.flightserver.repository.FlightRepository;
import com.kmia.bjs.flightserver.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Override
    public List<Flight> findAll() {
        return flightRepository.selectList(null);
    }

    @Override
    public Integer save(Flight flight) {
        return flightRepository.insert(flight);
    }

    @Override
    public Integer update(Flight flight) {
        return flightRepository.updateById(flight);
    }

    @Override
    public Integer deleteById(Integer id) {
        return flightRepository.deleteById(id);
    }

    @Override
    public List<Flight> findByFDate(String fDate) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fDate", fDate);
        return flightRepository.selectList(queryWrapper);
    }

}
