package com.kmia.bjs.flightserver.config;

import com.kmia.bjs.flightserver.util.ErrorUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerConfig {

    /**
     * 业务异常 统一处理
     */
    @ExceptionHandler(value = ServiceException.class)
    @ResponseBody
    public ResultBean exceptionHandler400(ServiceException e) {
        // 把错误信息输入到日志中
        log.error(ErrorUtil.errorInfoToString(e));
        return ResultBean.error(e.getErrorEnum().getCode(), e.getErrorEnum().getMsg());
    }

    /**
     * 空指针异常 统一处理
     */
    @ExceptionHandler(value = NullPointerException.class)
    @ResponseBody
    public ResultBean exceptionHandler500(NullPointerException e) {
        // 把错误信息输入到日志中
        log.error(ErrorUtil.errorInfoToString(e));
        return ResultBean.error(ErrorEnum.INTERNAL_SERVER_ERROR.getCode(), ErrorEnum.INTERNAL_SERVER_ERROR.getMsg());
    }

    /**
     * 未知异常 统一处理
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultBean exceptionHandler(Exception e) {
        // 把错误信息输入到日志中
        log.error(ErrorUtil.errorInfoToString(e));
        return ResultBean.error(ErrorEnum.UNKNOWN.getCode(), ErrorEnum.UNKNOWN.getMsg());
    }
}
