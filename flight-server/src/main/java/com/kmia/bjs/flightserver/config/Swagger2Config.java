package com.kmia.bjs.flightserver.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@EnableSwagger2 // 开启Swagger2的自动配置
@Configuration // 配置类
public class Swagger2Config {
    @Bean // 配置docket以配置Swagger具体参数
    public Docket docket(Environment environment) {
        // 设置要显示swagger的环境
        Profiles of = Profiles.of("prod", "test");
        // 判断当前是否处于该环境
        // 通过 enable() 接收此参数判断是否要显示
        boolean flag = environment.acceptsProfiles(of);
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(flag) //配置是否启用Swagger，如果是false，在浏览器将无法访问
                .groupName("默认分组")
                .select()
                //.apis(RequestHandlerSelectors.any())// api的配置路径
                .apis(RequestHandlerSelectors.basePackage("com.kmia.bjs.flightserver.controller"))// RequestHandlerselectors,配置要扫描接口的方式,basePackage:指定要扫描的包,any():扫描全部，none():不扫描
                .paths(PathSelectors.any()) // 扫描路径选择
                .build();
    }

    // 配置文档信息
    private ApiInfo apiInfo() {
        Contact contact = new Contact("赵刚晶", "联系人访问链接", "83877815@qq.com");
        return new ApiInfo(
                "公务机航班服务", // 标题
                "机组人员管理", // 描述
                "v1.0", // 版本
                "http://terms.service.url/组织链接", // 组织链接
                contact, // 联系人信息
                "Apach 2.0 许可", // 许可
                "许可链接", // 许可连接
                new ArrayList<>()// 扩展
        );
    }
}
