package com.kmia.bjs.flightserver.component;

import com.kmia.bjs.flightserver.service.impl.DepartureServiceImpl;
import com.kmia.bjs.flightserver.util.XMLUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


/**
 * @Description Kafka消费者
 * @Author 4K
 * @Date 2022/4/29 15:05
 * @Version 1.0
 **/
@Slf4j
@Component
public class KafkaConsumer {

    @Autowired
    private DepartureServiceImpl departureService;

    /**
     * @param
     * @return
     * @Description
     * @Version1.0
     * @author 4K
     * @date
     **/
    @KafkaListener(topics = "consoleTopic")
    public void processMessage(String content) {
        XMLUtil.writeXML(XMLUtil.formatXML(content));
        departureService.editFromXML(content);
    }
}
