package com.kmia.bjs.flightserver.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.kmia.bjs.flightserver.entity.Departure;

/**
 * @Description 出港航班服务接口
 * @Author 4K
 * @Date 2022/4/29 16:40
 * @Version 1.0
 **/
public interface IDepartureService<D> extends IService<Departure> {
}
