package com.kmia.bjs.flightserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @Description 机场实体
 * @Author 4K
 * @Date 2022/5/9 15:58
 * @Version 1.0
 **/
@Data
@ApiModel("机场实体")
@NoArgsConstructor// 创建无参的构造方法
@AllArgsConstructor// 创建满参的构造方法
@Accessors(chain = true)// 使用链式方法
@TableName("bas_location")// 对应表名
public class Location {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("ID，主键")
    private Integer id;

    @TableField("iataCode")
    @ApiModelProperty("三字码")
    private String iataCode;

    @TableField("icaoCode")
    @ApiModelProperty("四字码")
    private String icaoCode;

    @TableField("name")
    @ApiModelProperty("英文名称")
    private String name;

    @TableField("namechn")
    @ApiModelProperty("中文名称")
    private String namechn;

    @TableField("flightSector")
    @ApiModelProperty("航班类别 国际I 国内D 地区R")
    private String flightSector;

    @TableField("longitude")
    @ApiModelProperty("经度")
    private String longitude;

    @TableField("latitude")
    @ApiModelProperty("纬度")
    private String latitude;

    @TableField("opBy")
    @ApiModelProperty("操作人")
    private String opBy;

    @TableField("opAt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("操作时间")
    private Date opAt;

    @TableField("delFlag")
    @ApiModelProperty("删除标记 true or false")
    private Boolean delFlag;
}
