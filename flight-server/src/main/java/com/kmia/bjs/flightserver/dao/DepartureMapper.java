package com.kmia.bjs.flightserver.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kmia.bjs.flightserver.entity.Departure;

/**
 * @Description 出港数据库操作
 * @Author 4K
 * @Date 2022/4/29 16:49
 * @Version 1.0
 **/
public interface DepartureMapper extends BaseMapper<Departure> {
}
