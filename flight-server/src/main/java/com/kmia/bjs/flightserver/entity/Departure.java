package com.kmia.bjs.flightserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @Description 出港航班实体
 * @Author 4K
 * @Date 2022/4/29 15:05
 * @Version 1.0
 **/
@Data
@ApiModel("出港航班实体")
@NoArgsConstructor// 创建无参的构造方法
@AllArgsConstructor// 创建满参的构造方法
@Accessors(chain = true)// 使用链式方法
@TableName("bas_departure")// 对应表名
public class Departure {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("ID，主键")
    private Integer id;

    @TableField("SDT")
    @ApiModelProperty("航班计划营运日期")
    private Date SDT;

    @TableField("FLC")
    @ApiModelProperty("航班承运人，航空公司IATA代码")
    private String FLC;

    @TableField("IFC")
    @ApiModelProperty("航班承运人（ICAO）")
    private String IFC;

    @TableField("FLT")
    @ApiModelProperty("航班标识（由承运人、航班号、航班后缀组成）")
    private String FLT;

    @TableField("NFLT")
    @ApiModelProperty("新生成的航班标识，XX+4位数字，数字不以0开头")
    private String NFLT;

    @TableField("ORG")
    @ApiModelProperty("始发站IATA码")
    private String ORG;

    @TableField("ORGCHN")
    @ApiModelProperty("始发站(中文)")
    private String ORGCHN;

    @TableField("DESCHN")
    @ApiModelProperty("终点站机场（中文）")
    private String DESCHN;

    @TableField("DES")
    @ApiModelProperty("终点站机场（IATA）")
    private String DES;

    @TableField("STD")
    @ApiModelProperty("计划离港时间")
    private Date STD;

    @TableField("ETD")
    @ApiModelProperty("预计离港时间")
    private Date ETD;

    @TableField("ABT")
    @ApiModelProperty("起飞时间")
    private Date ABT;

    @TableField("ATD")
    @ApiModelProperty("实际撤轮档时间")
    private Date ATD;

    @TableField("CLA")
    @ApiModelProperty("飞行分类码 U/H 公务")
    private String CLA;

    @TableField("FST")
    @ApiModelProperty("航班服务类型")
    private String FST;

    @TableField("NAT")
    @ApiModelProperty("航班性质")
    private String NAT;

    @TableField("TOF")
    @ApiModelProperty("航段性质")
    private String TOF;

    @TableField("REG")
    @ApiModelProperty("飞机注册号")
    private String REG;

    @TableField("ITY")
    @ApiModelProperty("机型ICAO码")
    private String ITY;

    @TableField("TAR")
    @ApiModelProperty("机位")
    private String TAR;

    @TableField("OTC")
    @ApiModelProperty("营运类型代码（内部）")
    private String OTC;

    @TableField("GOT")
    @ApiModelProperty("登机门开始时间")
    private Date GOT;

    @TableField("STATE")
    @ApiModelProperty("航班状态 RF登机确认 NO正常 DE删除")
    private String STATE;

    @TableField("FLIGHTSTAUS")
    @ApiModelProperty("状态标识 AB   飞行中" +
            "BD   登机中" +
            "CX   取消航班" +
            "DV   改降" +
            "ES   估计时间 （Estimated的简写 表示该航班的所有时间状态都是预估的 ）" +
            "EX   飞机已从前场起飞" +
            "FB   行李递送开始" +
            "FS   最终进场" +
            "FX   航班完成" +
            "GA   登机门准备开发" +
            "SH   计划" +
            "GC   登机门关闭" +
            "GO   登机门开放" +
            "LB   最后一件行李" +
            "LC   最后呼叫" +
            "LD   着陆" +
            "NI   延误" +
            "OB   上/撤轮档" +
            "OT   按时" +
            "OV   着陆失败" +
            "RS   返回停机位" +
            "ZN   进入机场一定区域内（约12分钟内到达机场）" +
            "NO   非运营")
    private String FLIGHTSTAUS;

    @TableField("FLIGHTSTAUSCODE")
    @ApiModelProperty("状态标识代码")
    private String FLIGHTSTAUSCODE;

    @TableField("PAX")
    @ApiModelProperty("旅客总数")
    private String PAX;

    @TableField("FLIGHTREPEAT")
    @ApiModelProperty("重复号")
    private String FLIGHTREPEAT;

    @TableField("opBy")
    @ApiModelProperty("操作人")
    private String opBy;

    @TableField("opAt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("操作时间")
    private Date opAt;

    @TableField("delFlag")
    @ApiModelProperty("删除标记 true or false")
    private Boolean delFlag;
}
