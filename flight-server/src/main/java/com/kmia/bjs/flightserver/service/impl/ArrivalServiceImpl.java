package com.kmia.bjs.flightserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kmia.bjs.flightserver.dao.ArrivalMapper;
import com.kmia.bjs.flightserver.entity.Arrival;
import com.kmia.bjs.flightserver.service.IArrivalService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 进港航班服务
 * @Author 4K
 * @Date 2022/4/29 16:42
 * @Version 1.0
 **/
@Service
public class ArrivalServiceImpl extends ServiceImpl<ArrivalMapper, Arrival> implements IArrivalService<Arrival> {

    /**
     * @Description
     * @Version1.0
     * @author 4K
     * @date 2022/5/10 10:40
     * @param
     * @return
    **/
    public List<Arrival> findByFDate(String fDate) {
        QueryWrapper<Arrival> wrapper = new QueryWrapper<>();
        wrapper.eq("SDT", fDate);
        return super.list(wrapper);
    }

    /**
     * @Description 根据航班日期、航班号、重复号查询航班
     * @Version1.0
     * @author 4K
     * @date 2022/5/10 9:42
     * @param fDate flt flightRepeat
     * @return Departure
     **/
    public Arrival findByFDateFltFlightRepeat(String fDate, String flt, String flightRepeat) {
        QueryWrapper<Arrival> wrapper = new QueryWrapper<>();
        wrapper.eq("SDT", fDate);
        wrapper.eq("FLT", flt);
        wrapper.eq("FLIGHTREPEAT", flightRepeat);
        return super.getOne(wrapper);
    }
}
