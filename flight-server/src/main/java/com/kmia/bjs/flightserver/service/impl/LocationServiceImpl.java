package com.kmia.bjs.flightserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kmia.bjs.flightserver.dao.LocationMapper;
import com.kmia.bjs.flightserver.entity.Location;
import com.kmia.bjs.flightserver.service.ILocationService;
import org.springframework.stereotype.Service;

/**
 * @Description 机场服务
 * @Author 4K
 * @Date 2022/4/29 16:42
 * @Version 1.0
 **/
@Service
public class LocationServiceImpl extends ServiceImpl<LocationMapper, Location> implements ILocationService<Location> {

    /**
     * @Description 根据iataCode返回机场中文
     * @Version1.0
     * @author 4K
     * @date 2022/5/10 9:59
     * @param  iataCode
     * @return  机场中文
    **/
    public Location findByIataCode(String iataCode) {
        QueryWrapper<Location> wrapper = new QueryWrapper<>();
        wrapper.eq("iataCode", iataCode);
        return super.getOne(wrapper);
    }
}
