package com.kmia.bjs.flightserver.service;

import com.kmia.bjs.flightserver.entity.Flight;

import java.util.List;

public interface FlightService {
    List<Flight> findAll();

    Integer save(Flight flight);

    Integer update(Flight flight);

    Integer deleteById(Integer id);

    List<Flight> findByFDate(String fDate);
}