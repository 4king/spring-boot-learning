package com.kmia.bjs.flightserver.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kmia.bjs.flightserver.entity.Arrival;

/**
 * @Description 进港数据库操作
 * @Author 4K
 * @Date 2022/4/29 16:49
 * @Version 1.0
 **/
public interface ArrivalMapper extends BaseMapper<Arrival> {
}
