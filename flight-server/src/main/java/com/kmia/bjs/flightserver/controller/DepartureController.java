package com.kmia.bjs.flightserver.controller;

import com.kmia.bjs.flightserver.component.KafkaProducer;
import com.kmia.bjs.flightserver.config.ResultBean;
import com.kmia.bjs.flightserver.entity.Departure;
import com.kmia.bjs.flightserver.service.impl.DepartureServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("公务机出港航班服务API")
@RestController
@RequestMapping("/departure")
public class DepartureController {

    @Autowired
    private DepartureServiceImpl departureService;

    @Autowired
    private KafkaProducer kafkaProducer;

    @ApiOperation("Kafka发送消息")
    @RequestMapping(value = "/sendMessage", method = RequestMethod.GET)
    @ResponseBody
    public ResultBean sendMessage(String message) {
        kafkaProducer.send(message);
        return ResultBean.success();
    }

    /**
     * @param
     * @return
     * @Description 查询全部公务机航班
     * @Version1.0
     * @author 4K
     * @date
     **/
    @GetMapping("/query")
    @ApiOperation("查询全部公务机航班")
    public ResultBean getAll() {
        return ResultBean.success(departureService.list());
    }

    /**
     * @param fDate 航班日期
     * @return
     * @Description 根据航班日期查询公务机航班
     * @Version1.0
     * @author 4K
     * @date 2022/5/9 11:43
     **/
    @GetMapping("/queryByFDate")
    @ApiOperation("根据航班日期查询公务机航班")
    public ResultBean getByFNumberAndFDate(String fDate) {
        List<Departure> list = departureService.findByFDate(fDate, null);
        if (!list.isEmpty()) {
            return ResultBean.success(list);
        } else
            return ResultBean.success("无数据");
    }

    /**
     * @param departure 出港航班实体
     * @return
     * @Description 添加
     * @Version1.0
     * @author 4K
     * @date
     **/
    @PostMapping("/insert")
    @ApiOperation("新增公务机航班")
    public ResultBean insert(@RequestBody Departure departure) {
        if (departureService.save(departure)) {
            return ResultBean.success("新增公务机航班成功");
        }
        return ResultBean.error();
    }

    /**
     * @Description flightXML
     * @Version1.0
     * @author 4K
     * @date 2022/5/11 10:25
     * @param
     * @return
    **/
    @PostMapping("/xml/insert")
    @ApiOperation("xml新增公务机航班")
    public ResultBean insertByXML(String flightXML) {
        if (departureService.editFromXML(flightXML) != null) {
            return ResultBean.success("xml新增公务机航班成功");
        }
        return ResultBean.error();
    }

    /**
     * @param departure 出港航班实体
     * @return
     * @Description 修改
     * @Version1.0
     * @author 4K
     * @date
     **/
    @PutMapping("/update")
    @ApiOperation("修改公务机航班")
    public ResultBean update(@RequestBody Departure departure) {
        if (departureService.saveOrUpdate(departure)) {
            return ResultBean.success("修改公务机航班成功");
        }
        return ResultBean.error();
    }

    /**
     * @param id 航班ID
     * @return
     * @Description 删除
     * @Version1.0
     * @author 4K
     * @date 2022/5/9 11:42
     **/
    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除公务机航班")
    public ResultBean delete(@ApiParam("公务机航班id") @PathVariable("id") Integer id) {
        if (departureService.removeById(id)) {
            return ResultBean.success("删除公务机航班成功");
        }
        return ResultBean.error();
    }

}
