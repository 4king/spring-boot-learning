package com.kmia.bjs.flightserver.config;

/**
 * @Description 航班状态枚举
 * @Version1.0
 * @author 4K
 * @date 2022/5/9 16:29
 * @param
 * @return
**/
public enum FlightStateEnum {

    AB("AB", "飞行中"),
    BD("BD", "登机中"),
    CX("CX", "取消航班"),
    DV("DV", "改降"),
    ES("ES", "估计时间"),
    EX("EX", "飞机已从前场起飞"),
    FB("FB", "行李递送开始"),
    FS("FS", "最终进场"),
    FX("FX", "航班完成"),
    GA("GA", "登机门准备开放"),
    SH("SH", "计划"),
    GC("GC", "登机门关闭"),
    GO("GO", "登机门开放"),
    LB("LB", "最后一件行李"),
    LC("LC", "最后呼叫"),
    LD("FS", "着陆"),
    NI("NI", "延误"),
    OB("GA", "上/撤轮档"),
    OT("SH", "按时"),
    OV("OV", "着陆失败"),
    RS("GO", "返回停机位"),
    ZN("ZN", "进入机场一定区域内（约12分钟内到达机场）"),
    NO("NO", "NO");

    /**
     * 状态码
     */
    private String code;

    /**
     * 状态
     */
    private String des;

    FlightStateEnum(String code, String des) {
        this.code = code;
        this.des = des;
    }

    public String getCode() {
        return code;
    }

    public String getDes() {
        return des;
    }
}
