package com.kmia.bjs.flightserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.kmia.bjs.flightserver.dao"})// 哪一层继承了BaseMapper就对那一层进行扫描
public class FlightServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlightServerApplication.class, args);
	}

}
