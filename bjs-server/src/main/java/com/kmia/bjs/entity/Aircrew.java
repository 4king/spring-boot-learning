package com.kmia.bjs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;


@Data
@ApiModel("机组实体")
@NoArgsConstructor// 创建无参的构造方法
@AllArgsConstructor// 创建满参的构造方法
@Accessors(chain = true)// 使用链式方法
@TableName("bas_aircrew")// 对应表名
public class Aircrew implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)// 主键必须有TableId注解
    @ApiModelProperty("ID，主键")
    private Integer id;

    @TableField("name")
    @ApiModelProperty("姓名")
    private String name;

    @TableField("certType")
    @ApiModelProperty("证件类型 NI-身份证 PP-护照 AC-空勤证")
    private String certType;

    @TableField("cert")
    @ApiModelProperty("证件号码")
    private String cert;

    @TableField("duty")
    @ApiModelProperty("职务")
    private String duty;

    @TableField("fNumber")
    @ApiModelProperty("航班号")
    private String fNumber;

    @TableField("fDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @ApiModelProperty("航班日期")
    private Date fDate;

    @TableField("state")
    @ApiModelProperty("状态 NF未出行 NO正常")
    private String state;

    @TableField("adMark")
    @ApiModelProperty("进出港标识 D-出港 A-进港")
    private String adMark;

    @TableField("opBy")
    @ApiModelProperty("操作人")
    private String opBy;

    @TableField("opAt")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("操作时间")
    private Date opAt;

    @TableField("delFlag")
    @ApiModelProperty("删除标记 true or false")
    private Boolean delFlag;

}