package com.kmia.bjs.controller;

import com.kmia.bjs.config.ResultBean;
import com.kmia.bjs.entity.Aircrew;
import com.kmia.bjs.service.AircrewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("机组成员服务API")
@RestController
@RequestMapping("/aircrew")
public class AircrewController {

    @Autowired
    private AircrewService aircrewService;

    /**
     * 查询
     *
     * @return
     */
    @GetMapping("/query")
    @ApiOperation("查询全部机组人员")
    public ResultBean getAll() {
        return ResultBean.success(aircrewService.findAll());
    }

    /**
     * 根据航班号和航班日期查询机组人员
     *
     * @param fNumber 航班号
     * @param fDate   航班日期
     * @return
     */
    @GetMapping("/queryByFNumberAndFDate")
    @ApiOperation("根据航班号和航班日期查询机组人员")
    public ResultBean getByFNumberAndFDate(String fNumber, String fDate) {
        List<Aircrew> list = aircrewService.findByFNumberAndFDate(fNumber, fDate);
        if (!list.isEmpty()) {
            return ResultBean.success(list);
        }else
            return ResultBean.success("无数据");
    }

    /**
     * 添加
     *
     * @param aircrew
     * @return
     */
    @PostMapping("/insert")
    @ApiOperation("新增机组人员")
    public ResultBean insert(@RequestBody Aircrew aircrew) {
        Integer insert = aircrewService.save(aircrew);
        if (insert > 0) {
            return ResultBean.success("新增机组人员成功", insert);
        }
        return ResultBean.error();
    }

    /**
     * 修改
     *
     * @param aircrew
     * @return
     */
    @PutMapping("/update")
    @ApiOperation("修改机组人员")
    public ResultBean update(@RequestBody Aircrew aircrew) {
        Integer update = aircrewService.update(aircrew);
        if (update > 0) {
            return ResultBean.success("修改机组人员成功", update);
        }
        return ResultBean.error();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{id}")
    @ApiOperation("删除机组人员")
    public ResultBean delete(@ApiParam("机组人员id") @PathVariable("id") Integer id) {
        if (aircrewService.deleteById(id) > 0) {
            return ResultBean.success("删除机组人员成功");
        }
        return ResultBean.error();
    }

}
