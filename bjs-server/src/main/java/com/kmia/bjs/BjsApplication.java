package com.kmia.bjs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.kmia.bjs.repository")// 哪一层继承了BaseMapper就对那一层进行扫描
public class BjsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BjsApplication.class, args);
    }

}
