package com.kmia.bjs.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kmia.bjs.entity.Aircrew;
import org.springframework.stereotype.Repository;

@Repository
public interface AircrewRepository extends BaseMapper<Aircrew> {
}
