package com.kmia.bjs.service;

import com.kmia.bjs.entity.Aircrew;

import java.util.List;

public interface AircrewService {
    List<Aircrew> findAll();
    Integer  save(Aircrew aircrew);
    Integer  update(Aircrew aircrew);
    Integer  deleteById(Integer id);
    List<Aircrew> findByFNumberAndFDate(String fNumber, String fDate);
}