package com.kmia.bjs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kmia.bjs.entity.Aircrew;
import com.kmia.bjs.repository.AircrewRepository;
import com.kmia.bjs.service.AircrewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AircrewServiceImpl implements AircrewService {

    @Autowired
    private AircrewRepository aircrewRepository;

    @Override
    public List<Aircrew> findAll() {
        return aircrewRepository.selectList(null);
    }

    @Override
    public Integer save(Aircrew aircrew) {
        return aircrewRepository.insert(aircrew);
    }

    @Override
    public Integer update(Aircrew aircrew) {
        return aircrewRepository.updateById(aircrew);
    }

    @Override
    public Integer deleteById(Integer id) {
        return aircrewRepository.deleteById(id);
    }

    @Override
    public List<Aircrew> findByFNumberAndFDate(String fNumber, String fDate) {
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fNumber", fNumber);
        queryWrapper.eq("fDate", fDate);
        return aircrewRepository.selectList(queryWrapper);
    }

}
